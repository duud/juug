import { makeHtmlAttributes } from '@rollup/plugin-html'
let production = !process.env.ROLLUP_WATCH

let analytics = `
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-107042096-5', 'auto');
ga('send', 'pageview');
</script>
`

export default function({ attributes, bundle, files, publicPath, title }) {
  const scripts = (files.js || [])
    .map(({ fileName }) => {
      let attrs = makeHtmlAttributes(attributes.script)
      return `<script src="${publicPath}${fileName}"${attrs}></script>`
    })
    .join('\n')

  const links = (files.css || [])
    .map(({ fileName }) => {
      let attrs = makeHtmlAttributes(attributes.link)
      return `<link href="${publicPath}${fileName}" rel="stylesheet"${attrs}>`
    })
    .join('\n')

  let sentryScripts = production ? `
    <script src="https://browser.sentry-cdn.com/5.11.0/bundle.min.js" integrity="sha384-jbFinqIbKkHNg+QL+yxB4VrBC0EAPTuaLGeRT0T+NfEV89YC6u1bKxHLwoo+/xxY" crossorigin="anonymous"></script>
    <script>Sentry.init({ dsn: 'https://37d4eea5080646e193dd77ee8b7e36bc@sentry.io/1887312' });</script>
  ` : ''

  return `
<!DOCTYPE html>
<html ${makeHtmlAttributes(attributes.html)}>
  <head>
    <meta charset="utf-8" />
    <meta name='viewport' content='width=device-width,initial-scale=1'>
    <title>${title}</title>
    <link href="global.css" rel="stylesheet">
    ${links}
  </head>
  <body>
    ${analytics}
    ${sentryScripts}
    ${scripts}
  </body>
</html>
`
}

